//// Convert a TH2D of VLQ mass limit vs. BR into HEP data format (YAML)
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

#include "TFile.h"
#include "TH2D.h"
#include "TH2F.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "TGraph2D.h"


using namespace std;

string lumi = "36.1"; // integrated luminosity
string cme = "13"; // center of mass energy [GeV]

void dump_observed(std::vector<vector<double>> UL, string limit_type, std::ofstream &os){

  os << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: " << limit_type << "}" << endl;
  os << "  - {name: SQRT(S), units: TeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(u_int i = 0; i < UL[0].size(); ++i){
      if(UL[0][i]>1e-6) os << "  - {value: " << UL[0][i] << "}" << endl;  // the unphysical regions bin content set to 0
      else      os << "  - {value: \'-\'}" << endl;             // so only consider bins > 0
  }
}

void dump_expected_pm_1and2sigma(std::vector<vector<double>> UL, string limit_type, std::ofstream &os){

  // + error band
  os << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: Expected}" << endl;
  os << "  - {name: SQRT(S), units: TeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(int i = 0; i < UL[1].size(); ++i){
    os << "  - value: " << UL[1][i] << endl;
    os << "    errors: " << endl;
    os << "    - {asymerror: {plus: " << UL[2][i] << ", minus: " << -UL[4][i] << "}, label: '1 sigma'}" << endl;
    os << "    - {asymerror: {plus: " << UL[3][i] << ", minus: " << -UL[5][i] << "}, label: '2 sigma'}" << endl;
  }
}


void Limit2D_to_YAML( string fn1 ){

  cout << " Hello, I am your Heavy Neutrino YAML converter. Converting root file "<< fn1 << endl;

  string outfileName;
  cout << " Please tell me how to name your file " << endl;
  cin >> outfileName ;
  cout << " Will save you file with name " << outfileName << endl;
  std::ofstream os (outfileName, std::ofstream::out);

  TFile * _file1 = new TFile(fn1.c_str(),"OPEN");

  if(_file1==NULL){ cout << "Attention: the file you gave me is NULL. Please check. Now abort." << endl; return;}

  TGraph2D* h_observed = 0; TGraph2D* h_expected = 0;
  TGraph2D* h_expectedP1S = 0;   TGraph2D* h_expectedP2S = 0;
  TGraph2D* h_expectedM1S = 0;   TGraph2D* h_expectedM2S = 0;

  h_expected    = (TGraph2D*)_file1->Get("expectedUpperLimit_gr");
  h_expectedP1S    = (TGraph2D*)_file1->Get("expectedUpperLimitPlus1Sig_gr");
  h_expectedP2S    = (TGraph2D*)_file1->Get("expectedUpperLimitPlus2Sig_gr");
  h_expectedM1S    = (TGraph2D*)_file1->Get("expectedUpperLimitMinus1Sig_gr");
  h_expectedM2S    = (TGraph2D*)_file1->Get("expectedUpperLimitMinus2Sig_gr");
  h_observed    = (TGraph2D*)_file1->Get("upperLimit_gr");

  std::vector<TGraph2D*> Graphs;
  // Please remember filling order MATTERS.
  Graphs.push_back(h_observed);  Graphs.push_back(h_expected); Graphs.push_back(h_expectedP1S);
  Graphs.push_back(h_expectedP2S);  Graphs.push_back(h_expectedM1S); Graphs.push_back(h_expectedM2S);

  assert(h_observed); assert(h_expected); assert(h_expectedP1S); assert(h_expectedP2S); assert(h_expectedM1S); assert(h_observedM2S);

  int bins = Graphs[0]->GetN();

  std::vector<vector<double>> v_x;
  std::vector<vector<double>> v_y;
  std::vector<vector<double>> v_z;


  for(auto histo=0;histo<6;histo++){
      auto x = Graphs[histo]->GetX();
      auto y = Graphs[histo]->GetY();
      auto z = Graphs[histo]->GetZ();
      std::vector<double> tmpx;
      std::vector<double> tmpy;
      std::vector<double> tmpz;
      for(auto n=0; n<bins; n++){
        tmpx.push_back(x[n]);
        tmpy.push_back(y[n]);
        tmpz.push_back(z[n]);
      }
      v_x.push_back(tmpx);
      v_y.push_back(tmpy);
      v_z.push_back(tmpz);
      tmpx.clear();
      tmpy.clear();
      tmpz.clear();
  }


  os << "independent_variables:" << endl;

  os << "- header: {name: Mass $W_{R}$, units: TeV} {name: Mass $N_{R}$, units: TeV}" << endl;
  os << "  values:" << endl;
  for(int i = 0; i < v_x[0].size(); ++i){
        os << "  - {value: " << float(v_x[0][i])/1000. << "}" "  - {value: " << float(v_y[0][i])/1000. << "}"<< endl;
   }



  os << "dependent_variables:" << endl;

  dump_observed(v_z,"Observed",  os);
  dump_expected_pm_1and2sigma(v_z,"Expected", os);
  os.close();
}
