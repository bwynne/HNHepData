//// Convert a 1D limit plot into HEP data format (YAML) 
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

#include <TFile>
#include <TGraphAsymmErrors>
#include <string>
#include <stdio>

using namespace std;

string lumi = "36.1"; // integrated luminosity
string cme = "13000"; // center of mass energy [GeV]

void dump_limit(TGraph* g, string limit_type="Observed"){
  int nbins = g->GetN();

  cout << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  cout << "  qualifiers:" << endl;
  cout << "  - {name: Limit, value: " << limit_type << "}" << endl;
  cout << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  cout << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  cout << "  values:" << endl;
  
  for(int i = 0; i < nbins; ++i){
    cout << "  - {value: " << g->GetY()[i] << "}" << endl;
  }
}

void dump_expected_pm_1and2sigma(TGraphAsymmErrors* g1, TGraphAsymmErrors* g2){
  int nbins = g1->GetN();

  // + error band
  cout << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  cout << "  qualifiers:" << endl;
  cout << "  - {name: Limit, value: Expected}" << endl;
  cout << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  cout << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  cout << "  values:" << endl;

  for(int i = 0; i < nbins; ++i){
    cout << "  - value: " << g1->GetY()[i] << endl;
    cout << "    errors: " << endl;
    cout << "    - {asymerror: {plus: " << g1->GetEYhigh()[i] << ", minus: " << -g1->GetEYlow()[i] << "}, label: '1 sigma'}" << endl;
    cout << "    - {asymerror: {plus: " << g2->GetEYhigh()[i] << ", minus: " << -g2->GetEYlow()[i] << "}, label: '2 sigma'}" << endl;
  }
}

void Limit1D_to_YAML(string filename){
  
  
  TFile *_file1 = TFile::Open(filename.c_str());
  
  TGraph*               g_expected = 0;
  TGraphAsymmErrors*    gae_1sigma = 0;
  TGraphAsymmErrors*    gae_2sigma = 0;
  TGraph*               g_observed = 0;

  g_expected    = (TGraph*)_file1->Get("expected");
  gae_1sigma    = (TGraphAsymmErrors*)_file1->Get("1sigma");
  gae_2sigma    = (TGraphAsymmErrors*)_file1->Get("2sigma");
  g_observed    = (TGraph*)_file1->Get("observed");
  
  if(!g_expected){
    // alternative naming
    g_expected    = (TGraph*)_file1->Get("Graph3");
    gae_2sigma    = (TGraphAsymmErrors*)_file1->Get("Graph1");
    gae_1sigma    = (TGraphAsymmErrors*)_file1->Get("Graph2");
    g_observed    = (TGraph*)_file1->Get("Graph4");
  }
  
  assert(g_expected);
  assert(gae_1sigma);
  assert(gae_2sigma);
  assert(g_observed);

  int nbins = g_expected->GetN();

  TGraph* g = g_expected;
  
  cout << "independent_variables:" << endl;
  cout << "- header: {name: Mass, units: GeV}" << endl;
  cout << "  values:" << endl;
  for(int i = 0; i < nbins; ++i){
    cout << "  - {value: " << g->GetX()[i] << "}" << endl;
  }
  
  cout << "dependent_variables:" << endl;

  dump_limit(g_observed, "Observed");
  dump_expected_pm_1and2sigma(gae_1sigma, gae_2sigma);

}
